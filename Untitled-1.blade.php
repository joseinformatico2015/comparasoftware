

@foreach ($softwares as $software)

    <li class="list-item-new {{"software-"+$software->id}}" >
        <meta itemprop="position" content="0" />
        <span class="new-verified-badge">
        <img src="https://dev.comparasoftware.com/platform/images/verificado-green.png" alt="{{$software->nombre}}" />
        </span>
        <div class="row no-gutters">
            {{-- foto/estrellas --}}
            <div class="col-xs-12 col-sm-3">
                <div id="software-image-{{$software->id}}" class="software-image">
                    <a href="https://dev.comparasoftware.com/platform/{{$software->nombre}}">
                            <img alt="{{$software->nombre}}" class="img-thumbnail img-fluid lazy" data-src="{{$ruta=str_replace("public","storage",$software->imagen)}}" />
                    </a>
                </div>
                {{-- estrellas --}}
                <div class="col-bottom" style="margin-top:15px;">
                    <div class="rating text-center">
                        <span class="stars-rate">
                            <span class="stars-fill" style="width:50%;"></span>
                        </span>
                    </div>
                </div>
            </div>


        {{-- nombre/descripcion/link --}}
        <div class="col-xs-12 col-sm-7">
            <h2 class="title">
                <a href="{{$software->url}}" itemprop="url" class="ppc-provider-site" data-sid="1690" data-cid="140" data-source="click" >
                    <span itemprop="name">{{$software->nombre}}</span>
                    <i class="fa fa-external-link" style="font-size:17px;margin:0 0 0 10px;"></i>
                    </a>
            </h2>
            <div class="excerpt">
                {{$software->descripcion}}
                <a href="https://dev.comparasoftware.com/platform/fracttal">Ver mas de {{$software->nombre}}</a>
            </div>
            <div class="col-bottom">
                <a href="https://dev.comparasoftware.com/platform/{{$software->nombre}}" id="software-more-info-1690" class="">
                    Ver más información
                    <i class="fa "></i>
                </a>
                |
                <a href="javascript:;" id="software-prices-1690" class="btn-form"
                    data-code="Zm9ybS1wcmljZXM="
                    data-param_catid="140"
                    data-param_sid="1690">
                    Cotizar {{$software->nombre}}
                </a>
                |
                <a href="javascript:;" id="software-compare-1690" class="add-2-compare"
                    data-id="1690" data-label="Fracttal" data-href="https://dev.comparasoftware.com/platform/{{$software->nombre}}"
                    data-image="{{$ruta=str_replace("public","storage",$software->imagen)}}"
                    data-in='Comparando' data-out='+ Añadir a comparación'>
                    + Añadir a comparación
                </a>
            </div>
        </div>
            {{-- ir a la web --}}
            <div class="col-xs-12 col-sm-2">
            <div class="buttons" style="display:table-cell;vertical-align:middle;height:200px;text-align:center;">
                    <a href="{{$software->url}}" target="_blank" id="software-website-{{$software->id}}" class="btn btn-block ppc-provider-site" data-sid="{{$software->id}}" data-cid="140" data-source="click">
                        <i class="fa fa-external-link" style="font-size:25px;"></i>
                        <span class="d-block">Ir a la web</span>
                    </a>
            </div>
        </div>


        </div>

    </li>
@endforeach

<li class="list-item-new software-1690" >
    <meta itemprop="position" content="0" />
    <span class="new-verified-badge">
        <img src="https://dev.comparasoftware.com/platform/images/verificado-green.png" alt="Fracttal" />
    </span>
    <div class="row no-gutters">

        {{-- foto/estrellas --}}
        <div class="col-xs-12 col-sm-3">
            <div id="software-image-1690" class="software-image">
                <a href="https://dev.comparasoftware.com/platform/fracttal">
                    <img alt="Fracttal" class="img-thumbnail img-fluid lazy" data-src="/wp-content/uploads/2018/08/logoFracttal-Box-150x150.png" />
                </a>
            </div>
            {{-- estrellas --}}
            <div class="col-bottom" style="margin-top:15px;">
                <div class="rating text-center">
                    <span class="stars-rate">
                        <span class="stars-fill" style="width:50%;"></span>
                    </span>
                </div>
            </div>
        </div>
        {{-- nombre/descripcion/link --}}
        <div class="col-xs-12 col-sm-7">
            <h2 class="title">
                    <a href="https://www.fracttal.com/es/pide-tu-demo-con-nuestros-especialistas-compsoft" itemprop="url" class="ppc-provider-site" data-sid="1690" data-cid="140" data-source="click" >
                        <span itemprop="name">Fracttal</span>
                        <i class="fa fa-external-link" style="font-size:17px;margin:0 0 0 10px;"></i>
                    </a>
            </h2>
            <div class="excerpt">
                Fracttal es la plataforma de gestión de activos empresariales para Internet de las cosas (IoT). Fracttal es una solución completa de software y hardware que combina comunicación de máquina a máquina (M2M), telemática y una poderosa tecnología de gestión de activos. Líder mundial con orígen en Latinoamérica.
                <a href="https://dev.comparasoftware.com/platform/fracttal">Ver mas de Fracttal</a>
            </div>
            <div class="col-bottom">
                <a href="https://dev.comparasoftware.com/platform/fracttal" id="software-more-info-1690" class="">
                    Ver más información
                    <i class="fa "></i>
                </a>
                |
                <a href="javascript:;" id="software-prices-1690" class="btn-form"
                    data-code="Zm9ybS1wcmljZXM="
                    data-param_catid="140"
                    data-param_sid="1690">
                    Cotizar Fracttal
                </a>
                |
                <a href="javascript:;" id="software-compare-1690" class="add-2-compare"
                    data-id="1690" data-label="Fracttal" data-href="https://dev.comparasoftware.com/platform/fracttal"
                    data-image="/wp-content/uploads/2018/08/logoFracttal-Box.png"
                    data-in='Comparando' data-out='+ Añadir a comparación'>
                    + Añadir a comparación
                </a>
            </div>
        </div>
        {{-- ir a la web --}}
        <div class="col-xs-12 col-sm-2">
            <div class="buttons" style="display:table-cell;vertical-align:middle;height:200px;text-align:center;">
                    <a href="https://www.fracttal.com/es/pide-tu-demo-con-nuestros-especialistas-compsoft" target="_blank" id="software-website-1690" class="btn btn-block ppc-provider-site" data-sid="1690" data-cid="140" data-source="click">
                        <i class="fa fa-external-link" style="font-size:25px;"></i>
                        <span class="d-block">Ir a la web</span>
                    </a>
            </div>
        </div>
    </div>
</li>
