<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Softwares;
use GuzzleHttp\Client;

use App\Http\Controllers\SoftwareController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $softwares=Softwares::all();

        return view('/public')->with('softwares',$softwares);
    }
}
