<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use App\Softwares;


class SoftwareController extends Controller
{
    public function crear(){
        return view('/crear');
    }
    public function guardar(Request $request){

        $software=new Softwares();
        if (isset($request->imagen)) {
            $software->imagen=$request->file('imagen')->store('public');
        }
        $software->nombre=$request->nombre;
        $software->url=$request->url;
        $software->descripcion=$request->descripcion;
        $software->lenguaje=$request->lenguaje;
        $software->funcionalidad=$request->funcionalidad;
        $software->save();
        // dd($software);

        return redirect('/dashboard')->with('status', 'creado');
    }

    public function editar(){
        $softwares=Softwares::all();
        return view('/seleccionarSoftware')->with('softwares', $softwares);
    }

    public function editarSoftware($id){
        $software=Softwares::where('id',$id)->first();
        // dd($id, $software);
        return view('modals/modalEditar')->with('software',$software);
    }


    public function eliminarSoftware(Request $request){
        // dd($request);
        $software=Softwares::where('id',$request->solicitud_id)->first();
        $software->delete();
        return redirect('/editarSoftware')->with('status', 'eliminado');
    }

    public function modalEliminarSoftware($id){
        $software=Softwares::where('id',$id)->first();
        //   dd($id, $software);
        return view('modals/modalEliminar')->with('software',$software);
    }


    public function  guardarEditar(Request $request){
        // dd($request->all());
        $software=Softwares::where('id',$request->software_id)->first();

        if (isset($request->imagen)) {
            $software->imagen=$request->file('imagen')->store('public');
        }
        // dd($request->id);
        $software->nombre=$request->nombre;
        $software->url=$request->url;
        $software->descripcion=$request->descripcion;
        $software->lenguaje=$request->lenguaje;
        $software->funcionalidad=$request->funcionalidad;

        $software->save();

        return redirect('/editarSoftware')->with('status', 'editado');
    }

    public function apiAll(Request $request){

        $softwares=Softwares::all();
        return response()->json(['softwares'=>$softwares],200);
    }

}
