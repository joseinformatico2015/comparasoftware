@extends('layouts.adminlte')

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                @php $user = Auth::user(); @endphp
                <div class="card mt-5">
                    <div class="card-header">
                        <h3 class="card-title">Ingreso de nuevo Software</h3>

                        <div class="card-tools">
                            {{-- <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button> --}}
                        </div>
                    </div>
                    <div class="card-body">
                        <div >
                            <div class="col-md-12">
                                <form action="{{route('guardarSoftware')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="col-md-12">
                                        <label for="nombre">Nombre de Software  : <b style="color:red">(*)</b></label>
                                        <input type="text" class="form-control" name="nombre" id="nombre">
                                        <div class="input-group mb-3">
                                            @if ($errors->has('nombre'))
                                            <span class="text-danger ml-1">{{ $errors->first('nombre') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <label for="correo">Url de software  : <b style="color:red">(*)</b></label>
                                        <input type="text" class="form-control" name="url" id="url">
                                        <div class="input-group mb-3">
                                            @if ($errors->has('url'))
                                            <span class="text-danger ml-1">{{ $errors->first('url') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <label for="descripcion">Descripción de Software  : <b style="color:red">(*)</b></label>
                                        <textarea class="form-control" rows="4" cols="50" name="descripcion"style="display: block;margin-left: auto;margin-right: auto; " placeholder=" Debes Ingresar una descripcion"></textarea>
                                        <div class="input-group mb-3">
                                            @if ($errors->has('descripcion'))
                                            <span class="text-danger ml-1">{{ $errors->first('descripcion') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <label for="cedula">Imagen del Software: <b style="color:red"></b></label>
                                        <input type="file" class="form-control-file" name="imagen" id="imagen" aria-describedby="fileHelp">
                                        <b style="font-size: 11px; color:gray;">Extensiones permitidas :jpeg, png, jpg, bmp (MAX. 10mb)</b>
                                        <div class="input-group mb-3">
                                            @if ($errors->has('imagen'))
                                            <span class="text-danger ml-1">{{ $errors->first('imagen') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12"  style="display: block;">
                                        <label for="idioma">Lenguaje : <b style="color:red">(*)</b></label>
                                        <select class="form-control" name="idioma" id="idioma" >
                                            <option value="español" selected>Español</option>
                                            <option value="ingles" >Ingles</option>
                                            <option value="portuges" >Portuges</option>
                                        </select>
                                        <div class="input-group mb-3">
                                            @if ($errors->has('idioma'))
                                            <span class="text-danger ml-1">{{ $errors->first('idioma') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12"  style="display: block;">
                                        <label for="funcionalidad"> Funcionalidades Populares : <b style="color:red">(*)</b></label>
                                        <select class="form-control" name="funcionalidad" id="funcionalidad" >
                                            <option value="movil" selected>Acceso Móvil</option>
                                            <option value="preventivo">Mantenimiento Preventivo</option>
                                            <option value="predictivo" >Mantenimiento Predictivo</option>
                                            <option value="gestion">Gestión en terreno</option>
                                        </select>
                                        <div class="input-group mb-3">
                                            @if ($errors->has('funcionalidad'))
                                            <span class="text-danger ml-1">{{ $errors->first('funcionalidad') }}</span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="float-right">
                                            <button type="submit" class="btn btn-success mt-3">Enviar</button>
                                        </div>
                                    </div>
                                    <input type="hidden" id="fechas_input" name="fechas" value="">
                                    <input type="hidden" id="horas_input" name="horas" value="">

                                </form>
                            </div>
                        </div>
                    </div>
            </div>
            <!-- /.card -->
        </div>
    </div>
    </div>
</section>
@endsection
