<style>
    .botones {
      margin-top: 10px;
      display: flex;
      justify-content: space-around;
    }

    .btn_info {
      width: 100%;
    }

  </style>

  <div id="modalEditarSoftware" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Editar Software</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div class="row table-responsive mb-3">
            <div class="col-md-10" style="margin:auto">

              <form action="{{route('guardarEditarSoftware')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="col-md-12">
                    <label for="nombre">Nombre de Software  : <b style="color:red">(*)</b></label>
                    <input type="text" class="form-control" name="nombre" id="nombre" value="{{$software->nombre}}">
                    <div class="input-group mb-3">
                        @if ($errors->has('nombre'))
                        <span class="text-danger ml-1">{{ $errors->first('nombre') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-md-12">
                    <label for="correo">Url de software  : <b style="color:red">(*)</b></label>
                    <input type="text" class="form-control" name="url" id="url" value="{{$software->url}}">
                    <div class="input-group mb-3">
                        @if ($errors->has('url'))
                        <span class="text-danger ml-1">{{ $errors->first('url') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-md-12">
                    <label for="descripcion">Descripción de Software  : <b style="color:red">(*)</b></label>
                    <textarea class="form-control" rows="4" cols="50" name="descripcion" style="display: block;margin-left: auto;margin-right: auto; " placeholder=" Debes Ingresar una descripcion">{{$software->descripcion}}</textarea>
                    <div class="input-group mb-3">
                        @if ($errors->has('descripcion'))
                        <span class="text-danger ml-1">{{ $errors->first('descripcion') }}</span>
                        @endif
                    </div>
                </div>

                @if ($software->imagen!=null)
                        @php
                            $ruta=str_replace("public","storage",$software->imagen);
                            // dd($ruta,$software->imagen);
                        @endphp
                        <div class="col-md-12">
                            <input type="image" src={{$ruta}} alt="Submit" width="100" height="100">
                        </div>
                @endif

                <div class="col-md-12">
                    <label for="cedula">Imagen del Software: <b style="color:red"></b></label>
                    <input type="file" class="form-control-file" name="imagen" id="imagen" aria-describedby="fileHelp">
                    <b style="font-size: 11px; color:gray;">Extensiones permitidas :jpeg, png, jpg, bmp (MAX. 10mb)</b>
                    <div class="input-group mb-3">
                        @if ($errors->has('imagen'))
                        <span class="text-danger ml-1">{{ $errors->first('imagen') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-md-12"  style="display: block;">
                    <label for="idioma">Lenguaje : <b style="color:red">(*)</b></label>
                    <select class="form-control" name="idioma" id="idioma" >
                        <option value="español" selected>Español</option>
                        <option value="ingles" >Ingles</option>
                        <option value="portuges" >Portuges</option>
                    </select>
                    <div class="input-group mb-3">
                        @if ($errors->has('idioma'))
                        <span class="text-danger ml-1">{{ $errors->first('idioma') }}</span>
                        @endif
                    </div>
                </div>

                <div class="col-md-12"  style="display: block;">
                    <label for="funcionalidad"> Funcionalidades Populares : <b style="color:red">(*)</b></label>
                    <select class="form-control" name="funcionalidad" id="funcionalidad" >
                        <option value="movil" selected>Acceso Móvil</option>
                        <option value="preventivo">Mantenimiento Preventivo</option>
                        <option value="predictivo" >Mantenimiento Predictivo</option>
                        <option value="gestion">Gestión en terreno</option>
                    </select>
                    <div class="input-group mb-3">
                        @if ($errors->has('funcionalidad'))
                        <span class="text-danger ml-1">{{ $errors->first('funcionalidad') }}</span>
                        @endif
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="float-right">
                        <button type="submit" class="btn btn-success mt-3">Editar</button>
                    </div>
                </div>
                <input type="hidden" id="software_id" name="software_id" value="{{$software->id}}">

            </form>
            </div>



          </div>
        </div>



        <!-- Modal footer -->
        <div class="modal-footer">
          {{-- <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button> --}}
        </div>

      </div>
    </div>
  </div>

  <script>
        $(".editarSoftware").attr('disabled', false);

  </script>
