

<div id="modalEliminarSoftware" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="margin-top:10%">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">¿Seguro deseas eliminar este Software?</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <form action="{{route('eliminarSoftware')}}" method="post" id="eliminarSolicitud">
            @csrf
            <input type="hidden" name="solicitud_id" value="{{$software->id}}">
            <div class="form-row text-center">
              <div class="col-sm-12" style="margin-top:4%">
                <div class="col-sm-4"></div>
                <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i> Eliminar</button>
                <!-- <button type="button" data-dismiss="modal" class="btn btn-default"><i class="fas fa-undo-alt"></i> Cancelar</button> -->
              </div>
            </div>
          </form>
        </div>

      </div>
    </div>
  </div>

  <script>

  $(".eliminarSoftware").attr('disabled', false);

  </script>
