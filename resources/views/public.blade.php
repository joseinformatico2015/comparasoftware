<!-- select "contents".*, (select count(*) from "comments" where "contents"."id" = "comments"."object_id" and "object_type" = ? and "lang" = ?) as "comments_count" from "contents" inner join "content2category" on "contents"."id" = "content2category"."content_id" left join "content_meta" as "cm1" on "cm1"."content_id" = "contents"."id" and "cm1"."meta_key" = ? where "content2category"."category_id" = ? and "contents"."status" = ? order by cm1.meta_value::int desc, "comments_count" desc --><!DOCTYPE html>
<html lang="es">
<head>
	<link rel="canonical" href="https://dev.comparasoftware.com/platform/software-de-mantenimiento" />
<meta property="og:locale" content="es_ES" />
<meta property="og:type" content="object" />
<meta property="og:title" content="▷ Compara los Mejores Software De Mantenimiento【2020】" />
<meta property="og:description" content="Encuentra toda la información de Software de Mantenimiento | Lee reseñas de usuarios, cotiza precios y solicita demostraciones gratuitas | ✅ |" />
<meta property="og:url" content="" />
<meta property="og:site_name" content="@ComparaSoftware" />
<meta property="og:image" content="" />
<meta property="og:description" content="Encuentra toda la información de Software de Mantenimiento | Lee reseñas de usuarios, cotiza precios y solicita demostraciones gratuitas | ✅ |" />
<meta property="twitter:card" content="" />
<meta property="twitter:description" content="Encuentra toda la información de Software de Mantenimiento | Lee reseñas de usuarios, cotiza precios y solicita demostraciones gratuitas | ✅ |" />
<meta property="twitter:title" content="▷ Compara los Mejores Software De Mantenimiento【2020】" />
<meta property="twitter:site" content="@ComparaSoftware" />
<meta property="twitter:image" content="" />
	<script type='application/ld+json'>{"@context":"https:\/\/schema.org","@type":"Organization","url":"https:\/\/www.comparasoftware.com\/","sameAs":["https:\/\/web.facebook.com\/comparasoftware\/?_rdc=1\u0026_rdr","https:\/\/www.instagram.com\/compara.software\/","https:\/\/www.linkedin.com\/company\/comparasoftware\/","https:\/\/plus.google.com\/108731324786971504344","https:\/\/twitter.com\/ComparaSoftware"],"@id":"https:\/\/dev.comparasoftware.com\/platform\/#organization","name":"COMPARASOFTWARE","logo":"https:\/\/dev.comparasoftware.com\/platform\/images\/logo-800x131.png"}</script>
	<meta charset="utf-8" />
	<meta name="theme-color" content="#41A6DF" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<meta name="author" content="" />
	<link rel="icon" type="image/x-icon" href="https://dev.comparasoftware.com/platform/images/favicon-150x150.png" />
    <title>▷ Compara los Mejores Software De Mantenimiento【2020】</title>
     	<meta name="description" content="Encuentra toda la información de Software de Mantenimiento | Lee reseñas de usuarios, cotiza precios y solicita demostraciones gratuitas | ✅ |" />
		<link href="https://dev.comparasoftware.com/platform/css/styles.css" rel="stylesheet" />
	<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
	<link href="https://dev.comparasoftware.com/platform/style.css" rel="stylesheet" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
	<!--
	<script data-search-pseudo-elements defer src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
     -->
         <!-- jQuery -->
         <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.24.1/feather.min.js" crossorigin="anonymous"></script>

</head>
<body class="new-category category category-140 mid-4457 ">
<div id="layoutDefault">
	<div id="layoutDefault_content">
		<main>
			<nav class="navbar navbar-marketing navbar-expand-lg bg-transparent navbar-dark">
	<div class="container-fluid">
		<a href="https://dev.comparasoftware.com/platform">
			<img src="https://dev.comparasoftware.com/platform/images/logo-white.png" alt="" class="img-fluid nolazy" style="max-width:300px" />
		</a>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ml-auto mr-lg-5">
					<li class="nav-item">
						<a class="nav-link" href="/nuestra-empresa">Nuestra Empresa </a>
					</li>
					<li class="nav-item dropdown dropdown-xl no-caret">
						<a class="nav-link dropdown-toggle" id="navbarDropdownDemos" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Categorías de Software<i class="fa fa-chevron-right dropdown-arrow"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right animated--fade-in-up mr-lg-n15" aria-labelledby="navbarDropdownDemos">
							<div class="row no-gutters">
								<div class="col-lg-5 p-lg-3 bg-img-cover overlay overlay-primary overlay-70 d-none d-lg-block" style='background-image: url("https://www.comparasoftware.com/assets/img/screenshots/fondo-cat.jpeg")'>
									<div class="d-flex h-100 w-100 align-items-center justify-content-center">
										<div class="text-white text-center z-1">
											<div class="mb-3">Encuentra más de 150 categorías de software empresarial</div>
											<a class="btn btn-white btn-sm text-primary rounded-pill" href="https://dev.comparasoftware.com/platform/categorias">
												Ver todas las categorías
											</a>
										</div>
									</div>
								</div>
								<div class="col-lg-7 p-lg-5">
									<div class="row">
	                                                        <div class="col-lg-6">
	                                                            <h6 class="dropdown-header text-primary">Ventas</h6>
	                                                            <a class="dropdown-item" href="https://www.comparasoftware.com/software-crm">CRM</a><a class="dropdown-item" href="https://www.comparasoftware.com/punto-de-venta/">Punto de Ventas</a>
	                                                            <div class="dropdown-divider border-0"></div>
	                                                            <h6 class="dropdown-header text-primary">Marketing</h6>
	                                                            <a class="dropdown-item" href="https://www.comparasoftware.com/email-marketing/">Email Marketing</a><a class="dropdown-item" href="https://www.comparasoftware.com/chat-en-vivo/">Chat</a><a class="dropdown-item" href="https://www.comparasoftware.com/software-inmobiliario/">Real Estate</a>
	                                                            <div class="dropdown-divider border-0"></div>
	                                                            <h6 class="dropdown-header text-primary">Analitica</h6>
	                                                            <a class="dropdown-item" href="https://www.comparasoftware.com/inteligencia-de-negocio/">Ingelicencia de Negocio</a><a class="dropdown-item" href="https://www.comparasoftware.com/visualizacion-de-datos/">Dashboard</a>
	                                                            <div class="dropdown-divider border-0 d-lg-none"></div>
	                                                        </div>
	                                                        <div class="col-lg-6">
	                                                            <h6 class="dropdown-header text-primary">Recursos Humanos</h6>
	                                                            <a class="dropdown-item" href="https://www.comparasoftware.com/gestion-de-recursos-humanos/">Contrataciones</a><a class="dropdown-item" href="https://www.comparasoftware.com/gestion-de-recursos-humanos/">Manejo de RR.HH</a>
	                                                            <div class="dropdown-divider border-0"></div>
	                                                            <h6 class="dropdown-header text-primary">Fabricación</h6>
	                                                            <a class="dropdown-item" href="https://www.comparasoftware.com/software-erp/">ERP</a><a class="dropdown-item" href="https://www.comparasoftware.com/software-de-mantenimiento/">Mantenimiento</a><a class="dropdown-item" href="https://www.comparasoftware.com/sfc/">Control de Planta</a><a class="dropdown-item" href="https://www.comparasoftware.com/transporte/">Logistica</a>
	                                                        </div>
	                                                    </div>
								</div>
							</div>
						</div>
					</li>
				</ul>
				</div>
		</div>
	</div>
</nav>            									<header class="page-header page-header-dark bg-gradient-primary-to-secondary" style="padding-bottom:4rem;">
	<div class="page-header-content">
		<div class="container-fluid">
			<div class="row mx-lg-5 no-gutters">
				<div class="col-12 col-sm-12 col-md-8 col-lg-8">
					<h1 class="page-header-title">
						<h1 class="page-header-title">Software de Mantenimiento</h1>
					</h1>
					<p style="font-size:20px;">
						Cuando se habla del software de gestión de mantenimiento es común utilizar el término CMMS. Se trata del acrónimo de Computerized Maintenance Management Software-System. Asimismo, para referirse a este tipo de programa informático también suele...
						<a href="#category-content" onclick="jQuery('#category-tabs ul li:last-child a').tab('show');" style="color:#fff;">Leer m&aacute;s</a>
					</p>
					<p class="mb-2 mt-5" style="font-size:1.5rem;">¿Utilizas actualmente un Software de Mantenimiento?</p>
				</div>
			</div>
			<div class="row mx-lg-5 no-gutters">
				<div class="col-12 col-sm-12 col-md-8 col-lg-8">
					<div>
						<a class="btn btn-yes rounded-pill btn-form btn-header-yes btn-primary mb-3" href="#!"  style="margin-left:0;"
							data-param_catid="140" data-code="Zm9ybS1oZWFkZXIteWVz" data-param_gclid="">
							Sí
						</a>
						<a class="btn rounded-pill btn-no btn-form btn-header-no mb-3" href="#!"
							data-param_catid="140" data-code="Zm9ybS1oZWFkZXIteWVz" data-param_gclid="">
							No
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="svg-border-waves text-light">
		<svg class="wave" style="pointer-events: none" fill="currentColor" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1920 75">
			<defs>
				<style>
				.a {fill: none;}
				.b {clip-path: url(#a);}
				.d {opacity: 0.5;isolation: isolate;}
				</style>
				<clipPath id="a"><rect class="a" width="1920" height="75"></rect></clipPath>
			</defs>
			<title>wave</title>
			<g class="b"><path class="c" d="M1963,327H-105V65A2647.49,2647.49,0,0,1,431,19c217.7,3.5,239.6,30.8,470,36,297.3,6.7,367.5-36.2,642-28a2511.41,2511.41,0,0,1,420,48"></path></g>
			<g class="b"><path class="d" d="M-127,404H1963V44c-140.1-28-343.3-46.7-566,22-75.5,23.3-118.5,45.9-162,64-48.6,20.2-404.7,128-784,0C355.2,97.7,341.6,78.3,235,50,86.6,10.6-41.8,6.9-127,10"></path></g>
			<g class="b"><path class="d" d="M1979,462-155,446V106C251.8,20.2,576.6,15.9,805,30c167.4,10.3,322.3,32.9,680,56,207,13.4,378,20.3,494,24"></path></g>
			<g class="b"><path class="d" d="M1998,484H-243V100c445.8,26.8,794.2-4.1,1035-39,141-20.4,231.1-40.1,378-45,349.6-11.6,636.7,73.8,828,150"></path></g>
		</svg>
	</div>
</header>
<div id="category-tabs">
	<div class="container-fluid">
		<div class="row mx-lg-5 no-gutters">
			<div class="col">
				<div>
					<ul class="nav nav-tabs">
						<li class="nav-item">
							<a href="#category-listing" class="nav-link active" data-toggle="tab">Listado de software</a>
						</li>
						<li class="nav-item">
							<a href="#category-content" class="nav-link" data-toggle="tab">
								¿Qué es un Software de Mantenimiento?
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row mx-lg-5 no-gutters">
		<div class="col">
			<div class="tab-content">
				<div id="category-content" class="tab-pane">
					<div class="container-fluid">
						<div class="row justify-content-md-center">
							<div class="col-xs-12 col-sm-8">
								<h1>Software de Mantenimiento</h1>
								<div id=""><p>Software de mantenimiento &hellip; Cuando se habla del software de gesti&oacute;n de mantenimiento es com&uacute;n utilizar el t&eacute;rmino CMMS. Se trata del acr&oacute;nimo de <em>Computerized Maintenance Management Software-System</em>. Asimismo, para referirse a este tipo de programa inform&aacute;tico tambi&eacute;n suele hacerse uso de otros dos acr&oacute;nimos m&aacute;s. Puede utilizarse GMAO, por Gesti&oacute;n del Mantenimiento Asistido por Ordenador. Tambi&eacute;n se las siglas GMAC, por Gesti&oacute;n del Mantenimiento Asistido por Computador. En general, todas esas denominaciones coinciden en identificar a una herramienta que sirve para asumir el control del estado de los equipos e instalaciones de la empresa a fin de que funcionen cabalmente. Esto abarca el mantenimiento correctivo y preventivo, entre otros.</p>
<h3><strong>Software de gesti&oacute;n de mantenimiento para preservar el patrimonio de la empresa</strong></h3>
<p>Por su evoluci&oacute;n en el tiempo, ya no tiene solo como foco principal el objetivo que le da nombre. Ahora, tambi&eacute;n el software de gesti&oacute;n de mantenimiento puede ofrecer funciones adicionales que lo hacen ser a&uacute;n m&aacute;s eficaz. Sin importar sus dimensiones, el sector de la actividad econ&oacute;mica que las enmarque ni el producto o servicio que ofrezcan, todas las organizaciones pueden adquirir un software como este, que les resulte eficiente y que se ajuste a sus requerimientos.</p></div>
							</div>
													</div>
					</div>
				</div>
				<div id="category-listing" class="tab-pane active">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 order-md-2 order-lg-1">
                            <aside id="cartegory-sidebar-listing--" class="sidebar">
                                <div class="widget">
                                    <div id="filter-card" class="card">
                                        <div class="card-header"><h5 class="card-title">Filtros de Búsqueda</h5></div>
                                        <ul class="feature-box">
                                            <li>
                                                <a href="#feature-childs-1" data-toggle="collapse">
                                                    <i class="fa lenguage"></i> Lenguage
                                                </a>
                                                <ul id="feature-childs-1" class="feature-items collapse multi-collapse ">
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-2" type="checkbox" name="" value="2" />
                                                            <label class="custom-control-label filter" for="filter-cb-2">Español</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-3" type="checkbox" name="" value="3" />
                                                            <label class="custom-control-label filter" for="filter-cb-3">Ingles</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-4" type="checkbox" name="" value="4" />
                                                            <label class="custom-control-label filter" for="filter-cb-4">Portugues</label>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                        <ul class="feature-box">
                                            <li>
                                                <a href="#feature-childs-5" data-toggle="collapse">
                                                    <i class="fa modelo-de-precios"></i> Modelo de Precios
                                                </a>
                                                <ul id="feature-childs-5" class="feature-items collapse multi-collapse show">
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-6" type="checkbox" name="" value="6" />
                                                            <label class="custom-control-label filter" for="filter-cb-6">Prueba Gratuita</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-7" type="checkbox" name="" value="7" />
                                                            <label class="custom-control-label filter" for="filter-cb-7">Versión Gratuita</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-8" type="checkbox" name="" value="8" />
                                                            <label class="custom-control-label filter" for="filter-cb-8">Pago Mensual</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-9" type="checkbox" name="" value="9" />
                                                            <label class="custom-control-label filter" for="filter-cb-9">Pago anual</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-10" type="checkbox" name="" value="10" />
                                                            <label class="custom-control-label filter" for="filter-cb-10">Pago de única vez</label>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                        <ul class="feature-box">
                                            <li>
                                                <a href="#feature-childs-11" data-toggle="collapse">
                                                    <i class="fa despliegue"></i> Despliegue
                                                </a>
                                                <ul id="feature-childs-11" class="feature-items collapse multi-collapse ">
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-12" type="checkbox" name="" value="12" />
                                                            <label class="custom-control-label filter" for="filter-cb-12">Nube, SaaS, Web</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-13" type="checkbox" name="" value="13" />
                                                            <label class="custom-control-label filter" for="filter-cb-13">Instalado - Windows</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-14" type="checkbox" name="" value="14" />
                                                            <label class="custom-control-label filter" for="filter-cb-14">Instalado - Mac</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-15" type="checkbox" name="" value="15" />
                                                            <label class="custom-control-label filter" for="filter-cb-15">Instalado - Linux</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-16" type="checkbox" name="" value="16" />
                                                            <label class="custom-control-label filter" for="filter-cb-16">Dispositivo móvil - iOS Nativo</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-17" type="checkbox" name="" value="17" />
                                                            <label class="custom-control-label filter" for="filter-cb-17">Dispositivo móvil - Android Nativo</label>
                                                        </div>
                                                    </li>
                                                                        </ul>
                                            </li>
                                        </ul>
                                        <ul class="feature-box">
                                            <li>
                                                <a href="#feature-childs-18" data-toggle="collapse">
                                                    <i class="fa ind--strias"></i> Indústrias
                                                </a>
                                                <ul id="feature-childs-18" class="feature-items collapse multi-collapse ">
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-19" type="checkbox" name="" value="19" />
                                                            <label class="custom-control-label filter" for="filter-cb-19">Publicidad</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-20" type="checkbox" name="" value="20" />
                                                            <label class="custom-control-label filter" for="filter-cb-20">Agricultura</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-21" type="checkbox" name="" value="21" />
                                                            <label class="custom-control-label filter" for="filter-cb-21">concesionario de automóviles</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-22" type="checkbox" name="" value="22" />
                                                            <label class="custom-control-label filter" for="filter-cb-22">Bancario</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-23" type="checkbox" name="" value="23" />
                                                            <label class="custom-control-label filter" for="filter-cb-23">Construcción</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-24" type="checkbox" name="" value="24" />
                                                            <label class="custom-control-label filter" for="filter-cb-24">Consultante</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-25" type="checkbox" name="" value="25" />
                                                            <label class="custom-control-label filter" for="filter-cb-25">Distribución</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-26" type="checkbox" name="" value="26" />
                                                            <label class="custom-control-label filter" for="filter-cb-26">Educación</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-27" type="checkbox" name="" value="27" />
                                                            <label class="custom-control-label filter" for="filter-cb-27">Energía</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-28" type="checkbox" name="" value="28" />
                                                            <label class="custom-control-label filter" for="filter-cb-28">Ingenieria</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-29" type="checkbox" name="" value="29" />
                                                            <label class="custom-control-label filter" for="filter-cb-29">Bebida alimenticia</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-30" type="checkbox" name="" value="30" />
                                                            <label class="custom-control-label filter" for="filter-cb-30">Salud / Medicina</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-31" type="checkbox" name="" value="31" />
                                                            <label class="custom-control-label filter" for="filter-cb-31">Hostelería / Viajes</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-32" type="checkbox" name="" value="32" />
                                                            <label class="custom-control-label filter" for="filter-cb-32">Seguro</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-33" type="checkbox" name="" value="33" />
                                                            <label class="custom-control-label filter" for="filter-cb-33">Legal</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-34" type="checkbox" name="" value="34" />
                                                            <label class="custom-control-label filter" for="filter-cb-34">Servicio de Mantenimiento / Campo</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-35" type="checkbox" name="" value="35" />
                                                            <label class="custom-control-label filter" for="filter-cb-35">Fabricación</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-36" type="checkbox" name="" value="36" />
                                                            <label class="custom-control-label filter" for="filter-cb-36">Medios de comunicación</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-37" type="checkbox" name="" value="37" />
                                                            <label class="custom-control-label filter" for="filter-cb-37">Hipoteca</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-38" type="checkbox" name="" value="38" />
                                                            <label class="custom-control-label filter" for="filter-cb-38">Sin ánimo de lucro</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-39" type="checkbox" name="" value="39" />
                                                            <label class="custom-control-label filter" for="filter-cb-39">productos farmacéuticos</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-40" type="checkbox" name="" value="40" />
                                                            <label class="custom-control-label filter" for="filter-cb-40">Propiedad administrativa</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-41" type="checkbox" name="" value="41" />
                                                            <label class="custom-control-label filter" for="filter-cb-41">Sector público</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-42" type="checkbox" name="" value="42" />
                                                            <label class="custom-control-label filter" for="filter-cb-42">Bienes raíces</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-43" type="checkbox" name="" value="43" />
                                                            <label class="custom-control-label filter" for="filter-cb-43">Al por menor</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-44" type="checkbox" name="" value="44" />
                                                            <label class="custom-control-label filter" for="filter-cb-44">Software / TI</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-45" type="checkbox" name="" value="45" />
                                                            <label class="custom-control-label filter" for="filter-cb-45">Telecomunicaciones</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-46" type="checkbox" name="" value="46" />
                                                            <label class="custom-control-label filter" for="filter-cb-46">Transporte</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-47" type="checkbox" name="" value="47" />
                                                            <label class="custom-control-label filter" for="filter-cb-47">Utilidades</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-48" type="checkbox" name="" value="48" />
                                                            <label class="custom-control-label filter" for="filter-cb-48">Otros servicios</label>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                        <ul class="feature-box">
                                            <li>
                                                <a href="#feature-childs-49" data-toggle="collapse">
                                                    <i class="fa paises"></i> Paises
                                                </a>
                                                <ul id="feature-childs-49" class="feature-items collapse multi-collapse ">
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-50" type="checkbox" name="" value="50" />
                                                            <label class="custom-control-label filter" for="filter-cb-50">México</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-51" type="checkbox" name="" value="51" />
                                                            <label class="custom-control-label filter" for="filter-cb-51">España</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-52" type="checkbox" name="" value="52" />
                                                            <label class="custom-control-label filter" for="filter-cb-52">Colombia</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-53" type="checkbox" name="" value="53" />
                                                            <label class="custom-control-label filter" for="filter-cb-53">Chile</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-54" type="checkbox" name="" value="54" />
                                                            <label class="custom-control-label filter" for="filter-cb-54">Argentina</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-55" type="checkbox" name="" value="55" />
                                                            <label class="custom-control-label filter" for="filter-cb-55">Perú</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-56" type="checkbox" name="" value="56" />
                                                            <label class="custom-control-label filter" for="filter-cb-56">Ecuador</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-57" type="checkbox" name="" value="57" />
                                                            <label class="custom-control-label filter" for="filter-cb-57">Estados Unidos</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-58" type="checkbox" name="" value="58" />
                                                            <label class="custom-control-label filter" for="filter-cb-58">Guatemala</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-59" type="checkbox" name="" value="59" />
                                                            <label class="custom-control-label filter" for="filter-cb-59">Bolivia</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-60" type="checkbox" name="" value="60" />
                                                            <label class="custom-control-label filter" for="filter-cb-60">Costa Rica</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-61" type="checkbox" name="" value="61" />
                                                            <label class="custom-control-label filter" for="filter-cb-61">República Dominicana</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-62" type="checkbox" name="" value="62" />
                                                            <label class="custom-control-label filter" for="filter-cb-62">Venezuela</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-63" type="checkbox" name="" value="63" />
                                                            <label class="custom-control-label filter" for="filter-cb-63">Panama</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-64" type="checkbox" name="" value="64" />
                                                            <label class="custom-control-label filter" for="filter-cb-64">El Salvador</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-65" type="checkbox" name="" value="65" />
                                                            <label class="custom-control-label filter" for="filter-cb-65">Honduras</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-66" type="checkbox" name="" value="66" />
                                                            <label class="custom-control-label filter" for="filter-cb-66">Uruguay</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-67" type="checkbox" name="" value="67" />
                                                            <label class="custom-control-label filter" for="filter-cb-67">Paraguay</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-68" type="checkbox" name="" value="68" />
                                                            <label class="custom-control-label filter" for="filter-cb-68">Nicaragua</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-69" type="checkbox" name="" value="69" />
                                                            <label class="custom-control-label filter" for="filter-cb-69">Puerto Rico</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-70" type="checkbox" name="" value="70" />
                                                            <label class="custom-control-label filter" for="filter-cb-70">Brasil</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-71" type="checkbox" name="" value="71" />
                                                            <label class="custom-control-label filter" for="filter-cb-71">Cuba</label>
                                                        </div>
                                                    </li>
                                                                        </ul>
                                            </li>
                                        </ul>
                                        <ul class="feature-box">
                                            <li>
                                                <a href="#feature-childs-pf" data-toggle="collapse">
                                                    <i class="fa category-features"></i> front.popular_features
                                                </a>
                                                <ul id="feature-childs-pf" class="feature-items collapse multi-collapse show">
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-91" type="checkbox" name="" value="91" />
                                                            <label class="custom-control-label filter" for="filter-cb-91">Acceso Movil</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-92" type="checkbox" name="" value="92" />
                                                            <label class="custom-control-label filter" for="filter-cb-92">Mantenimiento Preventivo</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-93" type="checkbox" name="" value="93" />
                                                            <label class="custom-control-label filter" for="filter-cb-93">Mantenimiento Predictivo</label>
                                                        </div>
                                                    </li>
                                                                            <li>
                                                        <div class="custom-control custom-checkbox custom-control-solid filter">
                                                            <input class="custom-control-input filter-value" id="filter-cb-94" type="checkbox" name="" value="94" />
                                                            <label class="custom-control-label filter" for="filter-cb-94">Gestión en terreno</label>
                                                        </div>
                                                    </li>
                                                                        </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </aside>
                            <script>
                                (function()
                                {
                                    function doFilter()
                                    {
                                        let searchIds = [];
                                        document.querySelectorAll('#filter-card .filter-value:checked').forEach((cb) =>
                                        {
                                            searchIds.push(cb.value);
                                        });
                                        console.log('searchIds', searchIds);
                                        document.querySelectorAll('#software-list > li').forEach( (el) =>
                                        {
                                            if( searchIds.length <= 0 )
                                            {
                                                el.classList.remove('d-none');
                                            }
                                            else
                                            {
                                                let sf = JSON.parse(el.dataset.sf);
                                                console.log(sf);
                                                let contains = false;
                                                for(let sfid of searchIds)
                                                {
                                                    console.log(sfid);
                                                    if( sf.indexOf(sfid) != -1 )
                                                    {
                                                        contains = true;
                                                        break;
                                                    }
                                                }
                                                if( contains )
                                                    el.classList.remove('d-none');
                                                else
                                                    el.classList.add('d-none');
                                            }

                                        });
                                    }
                                    document.querySelectorAll('#filter-card .filter-value').forEach( (cb) =>
                                    {
                                        cb.addEventListener('click', function(e)
                                        {
                                            doFilter();
                                        });
                                    });
                                })();
                            </script>
                        </div>


						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 order-md-1 order-lg-2">
							<div class="row mb-3" style="font-size:20px;">
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div >
										<b style="color:#000;">{{$softwares->count()}}</b> apps en total
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
									<div class="text-right">
										Ordenar por:&nbsp;&nbsp;
										<select name="" class="d-inline">
											<option>Relevancia</option>
										</select>
									</div>
								</div>
							</div>

                            <ul id="software-list" class="list" itemscope itemtype="https://schema.org/ItemList">
                                <meta itemprop="numberOfItems" content="" />

{{--

                                <li class="list-item-new software-1690" >
                                    <meta itemprop="position" content="0" />
                                    <span class="new-verified-badge">
                                        <img src="https://dev.comparasoftware.com/platform/images/verificado-green.png" alt="Fracttal" />
                                    </span>
                                    <div class="row no-gutters">

                                        <div class="col-xs-12 col-sm-3">
                                            <div id="software-image-1690" class="software-image">
                                                <a href="https://dev.comparasoftware.com/platform/fracttal">
                                                    <img alt="Fracttal" class="img-thumbnail img-fluid lazy" data-src="/wp-content/uploads/2018/08/logoFracttal-Box-150x150.png" />
                                                </a>
                                            </div>

                                            <div class="col-bottom" style="margin-top:15px;">
                                                <div class="rating text-center">
                                                    <span class="stars-rate">
                                                        <span class="stars-fill" style="width:50%;"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-7">
                                            <h2 class="title">
                                                    <a href="https://www.fracttal.com/es/pide-tu-demo-con-nuestros-especialistas-compsoft" itemprop="url" class="ppc-provider-site" data-sid="1690" data-cid="140" data-source="click" >
                                                        <span itemprop="name">Fracttal</span>
                                                        <i class="fa fa-external-link" style="font-size:17px;margin:0 0 0 10px;"></i>
                                                    </a>
                                            </h2>
                                            <div class="excerpt">
                                                Fracttal es la plataforma de gestión de activos empresariales para Internet de las cosas (IoT). Fracttal es una solución completa de software y hardware que combina comunicación de máquina a máquina (M2M), telemática y una poderosa tecnología de gestión de activos. Líder mundial con orígen en Latinoamérica.
                                                <a href="https://dev.comparasoftware.com/platform/fracttal">Ver mas de Fracttal</a>
                                            </div>
                                            <div class="col-bottom">
                                                <a href="https://dev.comparasoftware.com/platform/fracttal" id="software-more-info-1690" class="">
                                                    Ver más información
                                                    <i class="fa "></i>
                                                </a>
                                                |
                                                <a href="javascript:;" id="software-prices-1690" class="btn-form"
                                                    data-code="Zm9ybS1wcmljZXM="
                                                    data-param_catid="140"
                                                    data-param_sid="1690">
                                                    Cotizar Fracttal
                                                </a>
                                                |
                                                <a href="javascript:;" id="software-compare-1690" class="add-2-compare"
                                                    data-id="1690" data-label="Fracttal" data-href="https://dev.comparasoftware.com/platform/fracttal"
                                                    data-image="/wp-content/uploads/2018/08/logoFracttal-Box.png"
                                                    data-in='Comparando' data-out='+ Añadir a comparación'>
                                                    + Añadir a comparación
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-2">
                                            <div class="buttons" style="display:table-cell;vertical-align:middle;height:200px;text-align:center;">
                                                    <a href="https://www.fracttal.com/es/pide-tu-demo-con-nuestros-especialistas-compsoft" target="_blank" id="software-website-1690" class="btn btn-block ppc-provider-site" data-sid="1690" data-cid="140" data-source="click">
                                                        <i class="fa fa-external-link" style="font-size:25px;"></i>
                                                        <span class="d-block">Ir a la web</span>
                                                    </a>
                                            </div>
                                        </div>
                                    </div>
								</li>

                                <li class="list-item-new software-4520" data-mid="41277" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" data-sfid_2="2"  data-sfid_3="3"  data-sfid_6="6"  data-sfid_8="8"  data-sfid_12="12" 	data-sf='["2","3","6","8","12"]'>
									<meta itemprop="position" content="1" />
									<span class="new-verified-badge">
										<img src="https://dev.comparasoftware.com/platform/images/verificado-green.png" alt="CMMS MP versión 10" />
									</span>
									<div class="row no-gutters">
										<div class="col-xs-12 col-sm-3">
											<div id="software-image-4520" class="software-image">
												<a href="https://dev.comparasoftware.com/platform/cmms-mp-version-10">
													<img alt="CMMS MP versión 10" class="img-thumbnail img-fluid lazy" data-src="/wp-content/uploads/2019/08/logomp-FB-150x150.png" />
												</a>
											</div>
											<div class="col-bottom" style="margin-top:15px;">
												<div class="rating text-center">
													<span class="stars-rate">
														<span class="stars-fill" style="width:84%;"></span>
													</span>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-7">
											<h2 class="title">
												<a href="https://dev.comparasoftware.com/platform/cmms-mp-version-10" itemprop="url">
													<span itemprop="name">CMMS MP versión 10</span>
												</a>
											</h2>
											<div class="excerpt">
												Redise&ntilde;amos MP con una nueva interfaz mucho m&aacute;s intuitiva y funcional, bases de datos m&aacute;s robustas y nuevas herramientas que har&aacute;n tu trabajo de gesti&oacute;n de mantenimi...
											</div>
											<div class="col-bottom">
												<a href="https://dev.comparasoftware.com/platform/cmms-mp-version-10" id="software-more-info-4520" class="">
													Ver más información
													<i class="fa "></i>
												</a>
												|
												<a href="javascript:;" id="software-prices-4520" class="btn-form"
													data-code="Zm9ybS1wcmljZXM="
													data-param_catid="140"
													data-param_sid="4520">
													Cotizar CMMS
												</a>
												|
												<a href="javascript:;" id="software-compare-4520" class="add-2-compare"
													data-id="4520" data-label="CMMS MP versión 10" data-href="https://dev.comparasoftware.com/platform/cmms-mp-version-10"
													data-image="/wp-content/uploads/2019/08/logomp-FB.png"
													data-in='Comparando' data-out='+ Añadir a comparación'>
													+ Añadir a comparación
												</a>
											</div>
										</div>
										<div class="col-xs-12 col-sm-2">
											<div class="buttons" style="display:table-cell;vertical-align:middle;height:200px;text-align:center;">
											</div>
										</div>
									</div>
								</li>

                                <li class="list-item-new software-1013" data-mid="11807"
									itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"
																											data-sf='[]'>
									<meta itemprop="position" content="2" />
																		<span class="new-verified-badge">
										<img src="https://dev.comparasoftware.com/platform/images/verificado-green.png" alt="eMaint CMMS" />
									</span>
																		<div class="row no-gutters">
										<div class="col-xs-12 col-sm-3">
											<div id="software-image-1013" class="software-image">
												<a href="https://dev.comparasoftware.com/platform/emaint-cmms">
																										<img alt="eMaint CMMS" class="img-thumbnail img-fluid lazy" data-src="/wp-content/uploads/2018/08/logoeMaint-CMMS-150x150.png" />
																									</a>
											</div>
											<div class="col-bottom" style="margin-top:15px;">
												<div class="rating text-center">
													<span class="stars-rate">
														<span class="stars-fill" style="width:86%;"></span>
													</span>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-7">
											<h2 class="title">
																								<a href="https://dev.comparasoftware.com/platform/emaint-cmms" itemprop="url">
													<span itemprop="name">eMaint CMMS</span>
												</a>
																							</h2>
											<div class="excerpt">
												eMaint CMMS ofrece plataformas innovadoras de fiabilidad de activos, que ayudarán a las organizaciones a aumentar su tiempo de actividad. Con una integración perfecta de herramientas de mantenimient...
																							</div>
											<div class="col-bottom">
												<a href="https://dev.comparasoftware.com/platform/emaint-cmms" id="software-more-info-1013" class="">
													Ver más información
													<i class="fa "></i>
												</a>
												|
												<a href="javascript:;" id="software-prices-1013" class="btn-form"
													data-code="Zm9ybS1wcmljZXM="
													data-param_catid="140"
													data-param_sid="1013">
													Cotizar eMaint
												</a>
												|
												<a href="javascript:;" id="software-compare-1013" class="add-2-compare"
													data-id="1013" data-label="eMaint CMMS" data-href="https://dev.comparasoftware.com/platform/emaint-cmms"
													data-image="/wp-content/uploads/2018/08/logoeMaint-CMMS.png"
													data-in='Comparando' data-out='+ Añadir a comparación'>
													+ Añadir a comparación
												</a>
											</div>
										</div>
										<div class="col-xs-12 col-sm-2">
											<div class="buttons" style="display:table-cell;vertical-align:middle;height:200px;text-align:center;">


																							</div>
										</div>
									</div>
								</li>

                                <li class="list-item-new software-2499" data-mid="25961"
									itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"
																											data-sf='[]'>
									<meta itemprop="position" content="3" />
																		<span class="new-verified-badge">
										<img src="https://dev.comparasoftware.com/platform/images/verificado-green.png" alt="IBM Maximo" />
									</span>
																		<div class="row no-gutters">
										<div class="col-xs-12 col-sm-3">
											<div id="software-image-2499" class="software-image">
												<a href="https://dev.comparasoftware.com/platform/ibm-maximo">
																										<img alt="IBM Maximo" class="img-thumbnail img-fluid lazy" data-src="/wp-content/uploads/2018/09/logoIBM-Maximo-150x150.png" />
																									</a>
											</div>
											<div class="col-bottom" style="margin-top:15px;">
												<div class="rating text-center">
													<span class="stars-rate">
														<span class="stars-fill" style="width:96%;"></span>
													</span>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-7">
											<h2 class="title">
																								<a href="https://dev.comparasoftware.com/platform/ibm-maximo" itemprop="url">
													<span itemprop="name">IBM Maximo</span>
												</a>
																							</h2>
											<div class="excerpt">
												IBM Maximo es la solución de gestión de activos empresariales líder a nivel mundial que le ayuda a optimizar su mantenimiento y obtener un mayor valor de sus activos. Gestione todos los tipos de ac...
																							</div>
											<div class="col-bottom">
												<a href="https://dev.comparasoftware.com/platform/ibm-maximo" id="software-more-info-2499" class="">
													Ver más información
													<i class="fa "></i>
												</a>
												|
												<a href="javascript:;" id="software-prices-2499" class="btn-form"
													data-code="Zm9ybS1wcmljZXM="
													data-param_catid="140"
													data-param_sid="2499">
													Cotizar IBM
												</a>
												|
												<a href="javascript:;" id="software-compare-2499" class="add-2-compare"
													data-id="2499" data-label="IBM Maximo" data-href="https://dev.comparasoftware.com/platform/ibm-maximo"
													data-image="/wp-content/uploads/2018/09/logoIBM-Maximo.png"
													data-in='Comparando' data-out='+ Añadir a comparación'>
													+ Añadir a comparación
												</a>
											</div>
										</div>
										<div class="col-xs-12 col-sm-2">
											<div class="buttons" style="display:table-cell;vertical-align:middle;height:200px;text-align:center;">


																							</div>
										</div>
									</div>
								</li>

                                <li class="list-item-new software-2944" data-mid="30801"
									itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"
																											data-sf='[]'>
									<meta itemprop="position" content="4" />
																		<span class="new-verified-badge">
										<img src="https://dev.comparasoftware.com/platform/images/verificado-green.png" alt="Neovero" />
									</span>
																		<div class="row no-gutters">
										<div class="col-xs-12 col-sm-3">
											<div id="software-image-2944" class="software-image">
												<a href="https://dev.comparasoftware.com/platform/neovero">
																										<img alt="Neovero" class="img-thumbnail img-fluid lazy" data-src="/wp-content/uploads/2018/11/Icone-Neovero2-150x150.jpg" />
																									</a>
											</div>
											<div class="col-bottom" style="margin-top:15px;">
												<div class="rating text-center">
													<span class="stars-rate">
														<span class="stars-fill" style="width:100%;"></span>
													</span>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-7">
											<h2 class="title">
																								<a href="https://dev.comparasoftware.com/platform/neovero" itemprop="url">
													<span itemprop="name">Neovero</span>
												</a>
																							</h2>
											<div class="excerpt">
												Software CMMS / EAM para gestión de Equipos Médicos ingeniería clínica / biomédica y mantenimiento hospitalario. Integrable en redes IoT para monitoreo de activos e Inteligencia Artificial. Neo...
																							</div>
											<div class="col-bottom">
												<a href="https://dev.comparasoftware.com/platform/neovero" id="software-more-info-2944" class="">
													Ver más información
													<i class="fa "></i>
												</a>
												|
												<a href="javascript:;" id="software-prices-2944" class="btn-form"
													data-code="Zm9ybS1wcmljZXM="
													data-param_catid="140"
													data-param_sid="2944">
													Cotizar Neovero
												</a>
												|
												<a href="javascript:;" id="software-compare-2944" class="add-2-compare"
													data-id="2944" data-label="Neovero" data-href="https://dev.comparasoftware.com/platform/neovero"
													data-image="/wp-content/uploads/2018/11/Icone-Neovero2.jpg"
													data-in='Comparando' data-out='+ Añadir a comparación'>
													+ Añadir a comparación
												</a>
											</div>
										</div>
										<div class="col-xs-12 col-sm-2">
											<div class="buttons" style="display:table-cell;vertical-align:middle;height:200px;text-align:center;">


																							</div>
										</div>
									</div>
								</li>

                                <li class="list-item-new software-3049" data-mid="31816"
									itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"
																											data-sf='[]'>
									<meta itemprop="position" content="5" />
																		<span class="new-verified-badge">
										<img src="https://dev.comparasoftware.com/platform/images/verificado-green.png" alt="EasyMaint" />
									</span>
																		<div class="row no-gutters">
										<div class="col-xs-12 col-sm-3">
											<div id="software-image-3049" class="software-image">
												<a href="https://dev.comparasoftware.com/platform/easymaint">
																										<img alt="EasyMaint" class="img-thumbnail img-fluid lazy" data-src="/wp-content/uploads/2018/12/logoEasymaint-150x150.png" />
																									</a>
											</div>
											<div class="col-bottom" style="margin-top:15px;">
												<div class="rating text-center">
													<span class="stars-rate">
														<span class="stars-fill" style="width:98%;"></span>
													</span>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-7">
											<h2 class="title">
																								<a href="https://dev.comparasoftware.com/platform/easymaint" itemprop="url">
													<span itemprop="name">EasyMaint</span>
												</a>
																							</h2>
											<div class="excerpt">
												EasyMaint es un software especializado en la Gestión de Mantenimiento de Activos, diseñado para incrementar la vida útil de equipos industriales, equipos médicos, equipos de carga y todo tipo de m...
																							</div>
											<div class="col-bottom">
												<a href="https://dev.comparasoftware.com/platform/easymaint" id="software-more-info-3049" class="">
													Ver más información
													<i class="fa "></i>
												</a>
												|
												<a href="javascript:;" id="software-prices-3049" class="btn-form"
													data-code="Zm9ybS1wcmljZXM="
													data-param_catid="140"
													data-param_sid="3049">
													Cotizar EasyMaint
												</a>
												|
												<a href="javascript:;" id="software-compare-3049" class="add-2-compare"
													data-id="3049" data-label="EasyMaint" data-href="https://dev.comparasoftware.com/platform/easymaint"
													data-image="/wp-content/uploads/2018/12/logoEasymaint.png"
													data-in='Comparando' data-out='+ Añadir a comparación'>
													+ Añadir a comparación
												</a>
											</div>
										</div>
										<div class="col-xs-12 col-sm-2">
											<div class="buttons" style="display:table-cell;vertical-align:middle;height:200px;text-align:center;">


																							</div>
										</div>
									</div>
								</li>

                                <li class="list-item-new software-4015" data-mid="41658" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" data-sf='[]'>
									<meta itemprop="position" content="6" />
																		<span class="new-verified-badge">
										<img src="https://dev.comparasoftware.com/platform/images/verificado-green.png" alt="WGM - Works Gestión de Mantenimiento" />
									</span>
									<div class="row no-gutters">
										<div class="col-xs-12 col-sm-3">
											<div id="software-image-4015" class="software-image">
												<a href="https://dev.comparasoftware.com/platform/wgm-works-gestion-de-mantenimiento">
																										<img alt="WGM - Works Gestión de Mantenimiento" class="img-thumbnail img-fluid lazy" data-src="/wp-content/uploads/2019/03/abismo-logo-150x150.png" />
																									</a>
											</div>
											<div class="col-bottom" style="margin-top:15px;">
												<div class="rating text-center">
													<span class="stars-rate">
														<span class="stars-fill" style="width:86%;"></span>
													</span>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-7">
											<h2 class="title">
												<a href="https://dev.comparasoftware.com/platform/wgm-works-gestion-de-mantenimiento" itemprop="url">
													<span itemprop="name">WGM - Works Gestión de Mantenimiento</span>
												</a>
											</h2>
											<div class="excerpt">
                                                 Abismo-net es una aplicación web de gestión de mantenimiento, multiusuario, multiplanta, multi-almacenes y multilingüe de última generación.
                                                    El objetivo principal de Abismo-net es realizar...
						                    </div>
											<div class="col-bottom">
												<a href="https://dev.comparasoftware.com/platform/wgm-works-gestion-de-mantenimiento" id="software-more-info-4015" class="">
													Ver más información
													<i class="fa "></i>
												</a>
												|
												<a href="javascript:;" id="software-prices-4015" class="btn-form"
													data-code="Zm9ybS1wcmljZXM="
													data-param_catid="140"
													data-param_sid="4015">
													Cotizar WGM
												</a>
												|
												<a href="javascript:;" id="software-compare-4015" class="add-2-compare"
													data-id="4015" data-label="WGM - Works Gestión de Mantenimiento" data-href="https://dev.comparasoftware.com/platform/wgm-works-gestion-de-mantenimiento"
													data-image="/wp-content/uploads/2019/03/abismo-logo.png"
													data-in='Comparando' data-out='+ Añadir a comparación'>
													+ Añadir a comparación
												</a>
											</div>
										</div>
										<div class="col-xs-12 col-sm-2">
											<div class="buttons" style="display:table-cell;vertical-align:middle;height:200px;text-align:center;">
										    </div>
										</div>
									</div>
								</li>

                                <li class="list-item-new software-3212" data-mid="33711"
									itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"
																											data-sf='[]'>
									<meta itemprop="position" content="7" />
																		<span class="new-verified-badge">
										<img src="https://dev.comparasoftware.com/platform/images/verificado-green.png" alt="TINC CMMS" />
									</span>
																		<div class="row no-gutters">
										<div class="col-xs-12 col-sm-3">
											<div id="software-image-3212" class="software-image">
												<a href="https://dev.comparasoftware.com/platform/tinc-cmms">
																										<img alt="TINC CMMS" class="img-thumbnail img-fluid lazy" data-src="/wp-content/uploads/2018/12/logoTINC-CMMS-150x150.png" />
																									</a>
											</div>
											<div class="col-bottom" style="margin-top:15px;">
												<div class="rating text-center">
													<span class="stars-rate">
														<span class="stars-fill" style="width:96%;"></span>
													</span>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-7">
											<h2 class="title">
																								<a href="https://dev.comparasoftware.com/platform/tinc-cmms" itemprop="url">
													<span itemprop="name">TINC CMMS</span>
												</a>
																							</h2>
											<div class="excerpt">
												TINC CMMS ofrece funciones de gestión de inventarios, administración de la información de mantenimiento y estrategias para la evaluación del equipo médico al alcance de cualquier PC, Laptop o Tab...
																							</div>
											<div class="col-bottom">
												<a href="https://dev.comparasoftware.com/platform/tinc-cmms" id="software-more-info-3212" class="">
													Ver más información
													<i class="fa "></i>
												</a>
												|
												<a href="javascript:;" id="software-prices-3212" class="btn-form"
													data-code="Zm9ybS1wcmljZXM="
													data-param_catid="140"
													data-param_sid="3212">
													Cotizar TINC
												</a>
												|
												<a href="javascript:;" id="software-compare-3212" class="add-2-compare"
													data-id="3212" data-label="TINC CMMS" data-href="https://dev.comparasoftware.com/platform/tinc-cmms"
													data-image="/wp-content/uploads/2018/12/logoTINC-CMMS.png"
													data-in='Comparando' data-out='+ Añadir a comparación'>
													+ Añadir a comparación
												</a>
											</div>
										</div>
										<div class="col-xs-12 col-sm-2">
											<div class="buttons" style="display:table-cell;vertical-align:middle;height:200px;text-align:center;">


																							</div>
										</div>
									</div>
								</li>
 --}}



                        @foreach ($softwares as $software)

                            <li class="list-item-new {{"software-".$software->id}}" >
                                <meta itemprop="position" content="0" />
                                <span class="new-verified-badge">
                                <img src="https://dev.comparasoftware.com/platform/images/verificado-green.png" alt="{{$software->nombre}}" />
                                </span>
                                <div class="row no-gutters">

                                    <div class="col-xs-12 col-sm-3">
                                        <div id="software-image-{{$software->id}}" class="software-image">
                                            <a href="https://dev.comparasoftware.com/platform/{{$software->nombre}}">
                                                    <img alt="{{$software->nombre}}" class="img-thumbnail img-fluid lazy" data-src="{{$ruta=str_replace("public","storage",$software->imagen)}}" />
                                            </a>
                                        </div>

                                        <div class="col-bottom" style="margin-top:15px;">
                                            <div class="rating text-center">
                                                <span class="stars-rate">
                                                    <span class="stars-fill" style="width:50%;"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                <div class="col-xs-12 col-sm-7">
                                    <h2 class="title">
                                        <a href="{{$software->url}}" itemprop="url" class="ppc-provider-site" data-sid="1690" data-cid="140" data-source="click" >
                                            <span itemprop="name">{{$software->nombre}}</span>
                                            <i class="fa fa-external-link" style="font-size:17px;margin:0 0 0 10px;"></i>
                                            </a>
                                    </h2>
                                    <div class="excerpt">
                                        {{$software->descripcion}}
                                        <a href="https://dev.comparasoftware.com/platform/fracttal">Ver mas de {{$software->nombre}}</a>
                                    </div>
                                    <div class="col-bottom">
                                        <a href="https://dev.comparasoftware.com/platform/{{$software->nombre}}" id="software-more-info-1690" class="">
                                            Ver más información
                                            <i class="fa "></i>
                                        </a>
                                        |
                                        <a href="javascript:;" id="software-prices-1690" class="btn-form"
                                            data-code="Zm9ybS1wcmljZXM="
                                            data-param_catid="140"
                                            data-param_sid="1690">
                                            Cotizar {{$software->nombre}}
                                        </a>
                                        |
                                        <a href="javascript:;" id="software-compare-1690" class="add-2-compare"
                                            data-id="1690" data-label="Fracttal" data-href="https://dev.comparasoftware.com/platform/{{$software->nombre}}"
                                            data-image="{{$ruta=str_replace("public","storage",$software->imagen)}}"
                                            data-in='Comparando' data-out='+ Añadir a comparación'>
                                            + Añadir a comparación
                                        </a>
                                    </div>
                                </div>

                                    <div class="col-xs-12 col-sm-2">
                                    <div class="buttons" style="display:table-cell;vertical-align:middle;height:200px;text-align:center;">
                                            <a href="{{$software->url}}" target="_blank" id="software-website-{{$software->id}}" class="btn btn-block ppc-provider-site" data-sid="{{$software->id}}" data-cid="140" data-source="click">
                                                <i class="fa fa-external-link" style="font-size:25px;"></i>
                                                <span class="d-block">Ir a la web</span>
                                            </a>
                                    </div>
                                </div>

                                </div>
                            </li>
                            @endforeach


							</ul>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<section class="category-bottom py-10">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-11">
				<div class="wrap-content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-lg-7">
								<h2 class="display-4 text-dark font-weight-800">¿Encontraste lo que buscabas?</h2>
								<p class="lead text-dark-50 mb-5">Si necesitas ayuda con tu búsqueda, no dudes en solicitar una asesoría.</p>
							</div>
							<div class="col-lg-5">
								<div class="text-right">
									<div class="mt-4"><a href="#" class="btn btn-primary">Necesito ayuda</a></div>
									<span class="text-dark-50">Puedes solicitar contacto en cualquier momento</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="svg-border-rounded text-dark">
    	<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 144.54 17.34" preserveAspectRatio="none" fill="currentColor">
    		<path d="M144.54,17.34H0V0H144.54ZM0,0S32.36,17.34,72.27,17.34,144.54,0,144.54,0"></path>
    	</svg>
	</div>
</section>
		</main>
	</div>
	<div id="layoutDefault_footer">
		<footer class="footer pt-10 pb-5 mt-auto bg-light footer-light">
			<div class="container">
				<div class="row">
					<div class="col-lg-3">
						<div class="footer-brand">ComparaSoftware SPA</div>
						<div class="mb-3">Ayudamos a empresas a tomar decisiones informadas sobre la elección de sus herramientas digitales.</div>
						<div class="icon-list-social mb-5">
							<a class="icon-list-social-link" target="_blank" rel="nofollow" href="https://www.instagram.com/compara.software/">
								<i class="fa fa-instagram"></i>
							</a>
							<a class="icon-list-social-link" target="_blank" rel="nofollow" href="https://www.facebook.com/comparasoftware/?_rdc=2&_rdr">
								<i class="fa fa-facebook"></i>
							</a>
							<a class="icon-list-social-link" target="_blank" rel="nofollow" href="https://www.linkedin.com/company/11402374/admin/">
								<i class="fa fa-linkedin"></i>
							</a>
							<a class="icon-list-social-link" target="_blank" rel="nofollow" href="https://twitter.com/ComparaSoftware">
								<i class="fa fa-twitter"></i>
							</a>
						</div>
					</div>
					<div class="col-lg-9">
						<div class="row">
							<div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
								<div class="text-uppercase-expanded text-xs mb-4">Nuestra empresa</div>
								<ul class="list-unstyled mb-0">
									<li class="mb-2"><a href="/nuestra-empresa">Sobre Nosotros</a></li>
									<li class="mb-2"><a href="https://blog.comparasoftware.com" target="blank">Blog</a></li>
									<li class="mb-2"><a href="javascript:void(0);">Sitemap</a></li>
									<li class="mb-2"><a href="https://contrataciones.comparasoftware.com/careers" rel="nofollow">Trabaja con nosotros</a></li>
								</ul>
							</div>
							<div class="col-lg-4 col-md-6 mb-5 mb-md-0">
									<div class="text-uppercase-expanded text-xs mb-4">Empresas de Software</div>
									<ul class="list-unstyled mb-0">
											<li class="mb-2"><a href="/nuestros-servicios">Nuestros Servicios</a></li>
                                            <li class="mb-2"><a href="https://soporte.comparasoftware.com/zs/XfbYIK">Registrar un software</a></li>
									        <li class="mb-2"><a href="javascript:void(0);">Iniciar sesion</a></li>
									</ul>
							</div>
							<div class="col-lg-4 col-md-6">
									<div class="text-uppercase-expanded text-xs mb-4">Contáctanos</div>
									<div class="mb-3">
										<p>info@comparasoftware.com</p>
										<p style="font-size: 13px;line-height: 0;">Chile +56-2-2958-9840</p>
										<p style="font-size: 13px;line-height: 0;">Argentina: +54-11-5279-8391</p>
										<p style="font-size: 13px;line-height: 0;">Colombia: +57-1-58023110</p>
										<p style="font-size: 13px;line-height: 0;">México: +52-55-8526-5801</p>
										<p style="font-size: 13px;line-height: 0;">Perú: +51-1-6429811</p><br>
										<p style="font-size: 13px;line-height: 0;">Perez Valenzuela, Piso 10</p>
										<p style="font-size: 13px;line-height: 0;">Santiago -Chile </p>
									</div>
							</div>
						</div>
					</div>
				</div>
				<hr class="my-5" />
				<div class="row align-items-center">
					<div class="col-md-6 small">ComparaSoftware SPA 2020</div>
					<div class="col-md-6 text-md-right small">
						<a href="javascript:void(0);">Políticas de Privacidad</a>
						&middot;
						<a href="javascript:void(0);">Políticas de Cookies</a>
						<a href="javascript:void(0);">Términos y Condiciones de uso</a>
					</div>
				</div>
			</div>
		</footer>
	</div><!-- end id="layoutDefault_footer" --></div>
<div id="comparator-bar">
	<div class="container-fluid">
		<div class="row no-gutters">
			<div class="col-10 col-sm-7">
				<div id="comparator-items" class="container-fluid"><div class="row no-gutters"></div></div>
			</div>
			<div class="col-2 col-sm-5">
				<div class="mt-3">
					<a href=" https://dev.comparasoftware.com/platform/comparador "
						id="btn-view-comparator" class="btn btn-secondary">
						<i class="fa fa-check"></i>
						<span class="text d-none d-sm-inline">Ver comparación</span>
					</a>
					<a href="javascript:;" class="btn-close-comparator btn">
						<i class="fa fa-close"></i>
						<span class="text d-none d-sm-inline">front.close_comparator_bar</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<!--
	<div id="comparator-header">
		<h3>Ver Comparación</h3>
		<a href="javascript:;" class="btn-close-comparator"><i class="fa fa-times-circle"></i></a>
	</div>
	<div id="comparator-body">
		<div class="container-fluid">
			<div class="col">

			</div>
		</div>
	</div>
	-->
</div>
<div id="modal-signin" class="modal fade">
	<form class="modal-dialog" action="https://dev.comparasoftware.com/platform/login" method="post">
		<input type="hidden" name="_token" value="68htH8sWPvm4WIVbJUxh4gvdhu8WhQfyzoI8pdFy">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Sign In</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
		        </button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>Username</label>
					<input type="text" name="username" value="" class="form-control" required />
				</div>
				<div class="form-group">
					<label>Password</label>
					<input type="password" name="password" value="" class="form-control" required />
				</div>
				<div class="form-group">
					<label>
						<input type="checkbox" name="remember_me" value="1" />
						Rememeber me
					</label>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        		<button type="submit" class="btn btn-primary">Sign In</button>
			</div>
		</div>
	</form>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js"></script>

<script>
    $( document ).ready(function() {
    console.log( "ready!" );

        axios.get('http://localhost:8000/api/softwareAll')
        .then(function (response) {
            appendToDOM(response.data);
            // response.data.forEach(appendToDOM);;
        })
        .catch(function (error) {
        console.log(error);
        })
        .then(function () {

        });
    });

function agregarSoftware(item, index) {
  document.getElementById("demo").innerHTML += index + ":" + item + "<br>";
}

const createLi = (user) => {
     const li = document.createElement('li');
    // add user details to `li`
    li.className="list-item-new software-"+`${user.id}`;
    var meta = document.createElement('meta');
    li.appendChild(meta);
    var span = document.createElement('span');
    var img = document.createElement('img');
    span.appendChild(img);
    var div = document.createElement('div');
    li.appendChild(span);
    li.appendChild(div);

    // li.textContent = `<div class="${user.id}"></div>${user.id}: ${user.first_name} ${user.last_name}`;
    // li.textContent = `  <meta itemprop="position" content="0" />
    //                     <span class="new-verified-badge">
    //                         <img src="https://dev.comparasoftware.com/platform/images/verificado-green.png" alt="${user.nombre}" />
    //                     </span>
    //                     <div class="row no-gutters">
    //                         <div class="col-xs-12 col-sm-3">
    //                             <div id="software-image-${user.id}" class="software-image">
    //                                 <a href="https://dev.comparasoftware.com/platform/${user.nombre}">
    //                                         <img alt="${user.nombre}" class="img-thumbnail img-fluid lazy" data-src="" />
    //                                 </a>
    //                             </div>

    //                             <div class="col-bottom" style="margin-top:15px;">
    //                                 <div class="rating text-center">
    //                                     <span class="stars-rate">
    //                                         <span class="stars-fill" style="width:50%;"></span>
    //                                     </span>
    //                                 </div>
    //                             </div>
    //                         </div>
    //                     </div>
    //                     `;

console.log(li);
    return li;
};

const appendToDOM = (users) => {
    console.log(users.softwares);
     const ul = document.getElementById('software-list');
    //iterate over all users
     users.softwares.map(user => {
        //  ul.appendChild(createLi(user));
        // createLi(user);
     });
};

const fetchUsers = () => {
    axios.get('https://reqres.in/api/users')
        .then(response => {
            const users = response.data.data;
            console.log(`GET list users`, users);
            // append to DOM
            // appendToDOM(users);
        })
        .catch(error => console.error(error));
};

// fetchUsers();


</script>



<script>
let jg = {"baseurl":"https:\/\/dev.comparasoftware.com\/platform","csrf_token":"68htH8sWPvm4WIVbJUxh4gvdhu8WhQfyzoI8pdFy","route_verify_number":"https:\/\/dev.comparasoftware.com\/platform\/formbuilder\/verifynumber"};
</script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

<script src="{{asset('/compara/scripts.js')}}"></script>
<script src="{{asset('/compara/front.js')}}"></script>
<script src="{{asset('/compara/category.js')}}"></script> --}}

{{-- <script src="https://dev.comparasoftware.com/platform/js/front.js"></script> --}}
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script>
AOS.init({disable: 'mobile', duration: 600, once: true});
</script>
 <script src="https://dev.comparasoftware.com/platform/js/category.js"></script>
<script>
(function()
{
	let f = document.querySelector('#layoutDefault_footer .footer');
	f.classList.remove('bg-light');
	f.classList.add('bg-dark');
})();
</script>
{{-- <script src="{{asset('forms.js')}}"></script>
<script src="{{asset('ppc.js')}}"></script> --}}
<script src="https://dev.comparasoftware.com/platform/js/modules/formbuilder/forms.js"></script>
	<script src="https://dev.comparasoftware.com/platform/js/modules/ppc/ppc.js"></script>

<script>
jQuery(function()
{
	let geodata = sessionStorage.getItem('geodata');
if( geodata )
{
	geodata = JSON.parse(geodata);
	document.querySelector('#filtro-form select[name=country]').value = geodata.country_name || '';
}
else
{
	document.addEventListener('geodata', function(e)
	{
		geodata = e.detail;
		document.querySelector('#filtro-form select[name=country]').value = geodata.country_name || '';
	});
}
	jQuery('.menu-item-sign-in').click(function()
	{
		jQuery('#modal-signin').modal('show');
	});
});
</script>
</body>
</html>
