@extends('layouts.adminlte')

@section('content')
<script src = "http://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js" defer ></script>
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                @php $user = Auth::user(); @endphp
                <div class="card mt-5">
                    <div class="card-header">
                        <h3 class="card-title">Elija el Software que desea editar</h3>

                        <div class="card-tools">
                            {{-- <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i></button> --}}
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12" style="padding:0px">
                            @if (session('status')=='creado')
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h5><i class="icon fas fa-check"></i> Atención!</h5>
                                La registro ha sido ingresado exitosamente!
                            </div>
                            @endif

                            @if (session('status')=='editado')
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h5><i class="icon fas fa-check"></i> Atención!</h5>
                                El registro ha sido actualizado exitosamente!
                            </div>
                            @endif

                        </div>
                        <table  class="table table-striped table-bordered table-sm nowrap" id="tabla_software" style="width: 100%">
                            <thead class="bold">
                                <tr>
                                    <td>Id</td>
                                    <td>Nombre</td>
                                    <td>Url</td>
                                    {{-- <td>Imagen</td> --}}
                                    <td>Lenguaje</td>
                                    <td>Funcionalidad</td>
                                    <td>Fecha creacion</td>
                                    <td>Acción</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($softwares as $key => $software)
                                    <tr>
                                        <td style=" text-align: center" >{{$software->id}}</td>
                                        <td>{{$software->nombre}}</td>
                                        <td>{{$software->url}}</td>
                                        {{-- <td>{{$software->imagen}}</td> --}}
                                        <td>{{$software->lenguaje}}</td>
                                        <td>{{$software->funcionalidad}}</td>
                                        <td>{{$software->created_at->format('d-m-Y')}}</td>


                                        <td style="text-align:center">
                                            <button type="button" class="btn btn-info btn-sm editarSoftware" name="{{$software->id}}"  title="editar software" onclick="editarSoftware(this.name)"><i class="fas fa-info-circle"></i></button>
                                            <button type="button" class="btn btn-danger btn-sm eliminarSoftware" name="{{$software->id}}"  onclick="eliminarSoftware(this.name)" title="eliminar software "><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
            </div>
            <!-- /.card -->
        </div>
    </div>
    </div>

    <div id="modal">
    </div>
</section>

<script>
 $(document).ready(function() {
        $('#tabla_software').DataTable({
            responsive: true,
            language: {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "No se encontraron registros",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "",
                "search": "Buscar:",
                "processing": "Consultando...",
                "paginate": {
                    "first": "Primera",
                    "last": "Ultima",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
            },
            "columns": [
        null,
        null,
        null,
        null,
        null,
        // null,
        null,
        null, //Added New

        // {"orderable": false, "width":"2%"},
    ],
        });


    });


    function eliminarSoftware(id) {
        $('.eliminarSoftware').attr('disabled', true);
        ruta = @json(route('modalEliminarSoftware', ['id' => 'id_prof']));
        ruta = ruta.replace('id_prof', id);
        console.log("aprete",ruta);

        $('.modal').modal('hide');
        $.get(ruta, function(data) {
            $('#modal').html(data);
            $('#modalEliminarSoftware').modal('show');
        });
        // setTimeout(() => {  $(".ELIMINAR").attr('disabled', false) }, 500);
    }

    function editarSoftware(id) {
        $(".editarSoftware").attr('disabled', true);
        ruta = @json(route('editarSoftware', ['id' => 'id_prof']));
        ruta = ruta.replace('id_prof', id);
        console.log("aprete",ruta);

        $('.modal').modal('hide');
            $.get(ruta, function(data) {
            //   console.log(data);
            $('#modal').html(data);
            $('#modalEditarSoftware').modal('show');
        });

    };

</script>
@endsection
