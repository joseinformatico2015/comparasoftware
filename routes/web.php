<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index')->name('index');
// Route::get('/public','HomeController@index')->name('index2');

Route::get('/crearSoftware','SoftwareController@crear')->name('crear');
Route::get('/editarSoftware','SoftwareController@editar')->name('editar');
Route::post('/eliminarSoftware','SoftwareController@eliminarSoftware')->name('eliminarSoftware');


route::get('/eliminarSoftware/{id}', 'SoftwareController@modalEliminarSoftware')->name('modalEliminarSoftware');

Route::post('/crearSoftwareGuardar','SoftwareController@guardar')->name('guardarSoftware');
Route::post('/guardarEditarSoftware','SoftwareController@guardarEditar')->name('guardarEditarSoftware');

Route::get('/admin', 'Auth\LoginController@showLoginForm')->name('admin');

Route::any('/login', 'Auth\LoginController@login')->name('login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::get('/editarSoftware/{id}', 'SoftwareController@editarSoftware')->name('editarSoftware');
